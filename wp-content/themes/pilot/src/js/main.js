(function() {

    wow = new WOW({
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        offset:       0,          // default
        mobile:       true,       // default
        live:         true        // default
    });
    wow.init();

})();
jQuery(document).ready(function($) {
    console.log( "Your JS is ready" );
	$('.main-navigation .sub-menu').parent().hover(function() {
		var submenu = $(this).children('.sub-menu');
		if ( $(submenu).is(':hidden') ) {
			$(submenu).show();
		} else {
			$(submenu).hide();
		}
	});
});



<?php

/**
 * Service Custom Post Type
 */
add_action( 'init', 'register_service_post_type');
function register_service_post_type()
{ 
	register_post_type( 'service',
		array( 'labels' => 
			array(
				'name'               => 'Services',
				'singular_name'      => 'Service',
				'all_items'          => 'All Services',
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add New Service',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Service',
				'new_item'           => 'New Service',
				'view_item'          => 'View Service',
				'search_items'       => 'Search Services',
				'not_found'          => 'Nothing found in the Database.',
				'not_found_in_trash' => 'Nothing found in Trash',
				'parent_item_colon'  => ''
			),
			'description'         => 'Services post type',
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'query_var'           => true,
			'menu_position'       => 10,
			'menu_icon'           => 'dashicons-service-alt',
			 'rewrite'	      => array( 'slug' => 'service', 'with_front' => false ),
			 'has_archive'      => 'services',
			'capability_type'     => 'page',
			'hierarchical'        => true,
			'supports'            => array( 'title', 'author', 'thumbnail', 'revisions')
		)
	);
}
/**
 */
register_taxonomy( 'service_cat', 
	array('service'),
	array('hierarchical' => true,
		'labels' => array(
			'name' 				=> 'Service Categories',
			'singular_name' 	=> 'Service Category',
			'search_items' 		=> 'Search Service Categories',
			'all_items' 		=> 'All Service Categories',
			'parent_item' 		=> 'Parent Service Category',
			'parent_item_colon' => 'Parent Service Category:',
			'edit_item' 		=> 'Edit Service Category',
			'update_item' 		=> 'Update Service Category',
			'add_new_item' 		=> 'Add New Service Category',
			'new_item_name' 	=> 'New Service Category Name',
		),
		'show_admin_column' => true, 
		'show_ui' 			=> true,
		'query_var' 		=> true,
		'rewrite' 			=> array( 'slug' => 'service-cat' ),
	)
);


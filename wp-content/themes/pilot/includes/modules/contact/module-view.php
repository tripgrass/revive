<?php 
	/**
	 * string		$args['title']
	 * int			$args['map_count']
	 * int			$args['zoom']						// from 0 (zoomed out) to 22 (zoomed in)
	 * float		$args['center_lat']
	 * float		$args['center_lng']
	 * array		$args['locations']
	 * string		$args['locations'][0]['address']	// in form of: 300 E 6th St, Tucson, AZ, United States
	 * float		$args['locations'][0]['lat']		// 32.22753
	 * float		$args['locations'][0]['lng']		// -110.96690100000001
	 * string		$args['locations'][0]['city']		// Tucson
	 * string		$args['locations'][0]['state']		// Arizon
	 * array		$args['markers']					// google map ready array of markers
	 */
	global $args;
?>
<div class="contact-wrap">

	<div class="contact-container">
		<div class="col-6 contact-left-container">
			
		</div>
		<div class="col-6 col-last contact-right-container">
			<div class="map">
				<div id='map-canvas-<?php echo $args['map_count']; ?>' class="map-canvas"></div>
			</div>
		</div>
	</div>


	<script>
		//<![CDATA[
		contact_maps = ( typeof contact_maps != 'undefined' && contact_maps instanceof Array ) ? contact_maps : [];
		allMarkers = ( typeof allMarkers != 'undefined' && allMarkers instanceof Array ) ? allMarkers : [];
		var this_map = {};
			this_map.map_count = <?php echo $args['map_count']; ?>;
			this_map.zoom = <?php echo $args['zoom']; ?>;
			this_map.center_lat = <?php echo $args['center_lat']; ?>;
			this_map.center_lng = <?php echo $args['center_lng']; ?>;
			this_map.markers = <?php echo json_encode( $args['markers'] ); ?>;
			contact_maps.push(this_map);
	//]]></script>
</div>
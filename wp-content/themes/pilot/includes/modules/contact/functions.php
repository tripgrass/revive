<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if (file_exists($filename)) {
    require $filename;
}
function build_contact_layout()
{
    global $pilot;
    $markers = array();
    $pilot->contact_map_count++;
    wp_enqueue_script('contact-map', get_template_directory_uri() . '/includes/modules/contact/module.js', array('jquery'), null, true);
    wp_register_script('gmap', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD5a-Y25lIt0feTJ7ydoJWUBJYF94OIYmo&v=3.exp&sensor=false', array(), '', true);
    wp_enqueue_script('gmap');

    if ($show_all = get_sub_field("contact_block_show_all")) {
        $locations = get_all_locations();
    } else {
        $locations = array();
        $location_posts = get_sub_field("contact_block_locations");
        if (is_array($location_posts)) {
            foreach ($location_posts as $post_id) {
                $location = new Location($post_id);
                $locations[] = $location;
            }
        }
    }
    $center_geo = get_sub_field("contact_block_map_center");
    if (is_array($center_geo)) {
        $center_lat = $center_geo["lat"];
        $center_lng = $center_geo["lng"];
        $markers = array(array(
            'name' => "Test",
            'lat' => $center_lat,
            'lng' => $center_lng,
            'address' => $center_geo["address"],
            'loc_id' => "",
            'icon' => "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC8AAABJCAYAAACtin/rAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAXEgAAFxIBZ5/SUgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAUQSURBVGiBzZtNbFVFFMd/My0GFW3AGNEW+VKxRLtAEnWjkSAmsvFjAcS404UJyoK4cOHOr6aGGFNkgRFjjAkJSNRUBBNkpRgtFIIotBBDIaCpQRNsFeH+Xdx5ze3r+7gzd27LP3l5uTNzzvm/03Pm48ytMYgiSIQBlgGPA53ALZlPG/AH8FvmcxDos4bBQoYBE0LeEV4NPOlIzw2wPQj0Adut4UCAPBjk9ZH0hKQBxcVuSct9ufiQXiWpPzLpauyS1BmNvCQr6Y2SSWcxKunZwuQlzVb6J50O9Eqa0fAH1EvYRNwBfAUsDkqmOPgOWG0NF2p11iSfiDbge2BJudxyYR/wmDVcru6w1Q2JaAG2c3UQB1gB9NbsqRHn70xTjDfDhoYxn4iVwNdT41BvXAG6rOFYpWE8bNyq2R3JkIAh0hV0yD0XRQvQM6ElEy5rC/5ZL0nqlvSIpLaqUGxz7d1uXBGsnDDPS5ohaaiAwn5JXTlX6i4VW6kPS7JZ8k8VUPaWpFbP/VGrkwvFqiz5jwKV7JVkfDdUzqZx8iHYYhBGUivwOzDbM4H+Au6xhjOhGZiIDuAo6b7fB+eBdgs8hD9xgI1FiAM4+Y0BonOBBy3waKDtnYFysfSsssD8AMGT1vBnoNEJcHpOBojOt0B7gOCPATKx9c2zwG0Bgj8FyMTW1xHq+YUBMrH1dVhgNEBweYBMbH3/WWA4QHBpIq4NkJsEp2dpgOh5C5wOEGwhPSTEwAqnzxfnQj0P0JuIGwJlAXDytU9JzXE81PMAC4BNgbIVbHJ6QrAPSWsCN0cVrAvcmK0rYDORdDOS5kgaK/gDtqnqANKAdJsbXwQHslvijwsqk6RhpeeCWXVIz3L9wxFsrRk/gCfiYWB/wfitIAGOA/3uewlwn/ueVGoJwCngLmu4Ml49SMQvXD21mkZYbw2bYaIn3p8mMj4YAT6oPGTJbwXOTTkdP/Raw1jlobro9DSwYzpY5cBFYKE1jFQaJiSQNewEPptqVjnxapY41KgSJ6Id+BmKLf2R8QPwgDUk2cZJU5c1nAVemSpWOXAZeK6aONSfd7cA35ZKKT96rOFIrY5GNyOLgEPAjSUSa4ZB0srwP7U666541nAKeKEsVjkg4Pl6xIFct4EfRtiLhKCn6UavXthUkIhZpFfud0b1a2MMAPdbw6VGg5pulKzhIrAWGiuKiDHgmWbEIecuzxoOMnXT58vZq5uG8CxJl32h3Od1ImsW81kk4nbS1fe6MKc2xChwtzX5CwJehwNrOA287ssqJ17zIQ4B79sk4hrSC4GYs88J4N48SZqF97HMGXjJV64J1vsSB/xfFsok8K5ISbojlEPQa1oAiVgAHINCNcu/gU7fWK8g+DRvDb8Cb4bKO3gnaRbBngdIxEzSi4FFAeLHSXeMwSt3oTqK2/FtCBR/sQhxIDxhq5L3S88k/TSG3RgVLPB/W+TtGEYLxXwWiTgMdOUYesgalsWwGcvzAO/mHLc5lsGYnp8JnAFuajDsAtBhTdAl3iRE87ybebY2GbYtFnGI6HmARMwjLUG31ugWaWl6KJa9mDGPWy131eneE5M4RCbvUO92773YhqKGDYy/JXgWuDXTPArMsYZ/Y9qK7vn0zTW+qGr+JjZxKCdsAD6vet5dhpHoYQPjc/4IcL1rWuzKh1FRiufdnL/XPZ4ogziUFzYAe9z3/rIMlEm+8h84A2UZKJP8UeATJidvNPwP18d6NRI2QawAAAAASUVORK5CYII="
        ));
    } else {
        $center_lat = "39.5";
        $center_lng = "98.35";
    }
    $zoom = get_sub_field("contact_block_zoom");
    if (!$zoom) {
        $zoom = 4;
    }
    $args = array(
        "map_count" => $pilot->contact_map_count,
        "zoom" => $zoom,
        "center_lat" => $center_lat,
        "center_lng" => $center_lng,
        "locations" => $locations,
        "markers" => $markers
    );
    return $args;
}

?>
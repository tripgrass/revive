<?php
	$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}

	function build_header_layout(){
		if( is_home() || is_front_page() ){
			global $i;
			$link = "";
			$link_post = get_sub_field('header_block_link');
			if( is_object( $link_post ) ){
				$link = get_permalink($link_post->ID);
			}
			$block_width = get_sub_field('header_block_width');
			$custom_classes = " " . preg_replace('/_/','-',$block_width) . " " . get_sub_field('header_block_custom_class');
			$button_object = ( get_sub_field('header_block_button_href') ? get_sub_field('header_block_button_href') : get_sub_field('header_block_custom_button_href') );
			if( is_object( $button_object ) ){
				$button_href = get_permalink( $button_object->ID);
			}
			else{
				$button_href = $button_object;
			}
			$header_args = array(
				'width' => $block_width,
				'width_class' => preg_replace('/_/','-',$block_width),
				'subtitle' => get_sub_field('header_block_subtitle'),
				'button_text' => get_sub_field('header_block_button_text'),
				'button_href' => $button_href,
				'id' => 'header_block_'.$i,
				'overlay_color' => '',
				'overlay_opacity' => '',
				'left_align_title' => get_sub_field('left_align_title')
	
			);
			if( 'title' == get_sub_field('header_block_logo_or_title') ){
				$header_args['title'] = get_sub_field('header_block_title');
			}
			else{
				$header_args['logo'] = get_sub_field('header_block_logo');
			}
			if( get_sub_field('header_block_modify') ){
				if( $opacity = get_sub_field('header_block_overlay_opacity') ){
					$header_args['overlay_opacity'] = $opacity;
				}
				if( $color = get_sub_field('header_block_overlay_color') ){
					$header_args['overlay_color'] = $color;
				}
			}				
			$type = get_sub_field('header_block_type');
			if( $type == 'image' ){
				$image = get_sub_field('header_block_image');
				if( is_array( $image ) ){
					$header_args['image_url'] = $image['url'];
				}
			}
			if( $type == 'video_url' ){
				$mp4_file = get_sub_field('header_video_file_mp4');
				$fallbackImage = get_sub_field('header_block_video_fallbackimage');
				if( is_array( $mp4_file ) ){
					$header_args['video_file_mp4'] = $mp4_file['url'];
					$header_args['fallback_image_url'] = $fallbackImage['url'];
				}
			}
			$args[] = $header_args;
			if( is_array( $args ) ){
				return $args;
			}
		}
	}
?>
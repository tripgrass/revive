<footer class="site-footer">
	<div class="container">
		<div class="footer-info">
			<div class="col">
				<label>CONTACT US</label>
				<ul>
					<li>CALL: <a href="tel:5204291522">520-429-1522</a></li>
					<li>Email: <a href="mailto:jason@revive.com" target="_blank">jason@revive.com</a></li>
				</ul>
			</div>
			<div class="col">
				<label>VISIT US</label>
				<ul>
					<li>2404 E River Rd</li>
					<li>Building 1</li>
					<li>Tucson, AZ</li>
					<li><a href="/contact">View Map</a></li>
				</ul>
			</div>
			<div class="col">
<!--				<label>FOLLOW US</label> -->
			</div>
		</div>
		<div class="footer-action">
<!--			<a href="" class="button">Join Our Mailing List</a> -->
			<a href="/schedule" class="button">Schedule Now!</a>
		</div>
		<div class="site-info">
			<?php echo date("Y"); ?>&copy; <?php printf( esc_html__( 'Design by %1$s', 'pilot' ), '<a href="http://www.nikicortiz.com/kanikanik-gallery/">KaniKanik</a>' ); ?>
		</div><!-- .site-info -->
	</div>
</footer>
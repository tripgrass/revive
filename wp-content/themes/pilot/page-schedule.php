<?php 
/*
Template Name: Schedule
*/
get_header(); ?>
<div class="sched-wrap container" >
	<div class="scheduler">
		<link href='https://fdhq-assets.s3.amazonaws.com/widget/appointment/bundle.css' rel='stylesheet'>
		<span>
		<script defer='defer' src='https://fdhq-assets.s3.amazonaws.com/widget/appointment/bundle.min.js'></script>
		</span>
		<div class='fd-appointment' data-fd-domain='rpr' data-fd-timezone='America/Phoenix' data-location='27125' data-ng-cloak='' data-ui-view='' ng-controller='contextController'></div>
	</div>
</div>

		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'views/content', 'page' ); ?>
			<?php pilot_get_comments(); ?>
		<?php endwhile; ?>

<?php get_footer(); ?>